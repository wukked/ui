﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace UI
{
    public partial class UIForm : Form
    {
        public UIForm()
        {
            InitializeComponent();
            //Ensure headerVisualStyles set to false
            //Set column header back color
            this.rightDGV.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#EBEADB");
            //Enable row selected back color
            this.rightDGV.DefaultCellStyle.SelectionBackColor = System.Drawing.ColorTranslator.FromHtml("#F2F2D2");
            this.rightDGV.DefaultCellStyle.SelectionForeColor = System.Drawing.ColorTranslator.FromHtml("#000000"); 


            //this.rightDGV.CellBorderStyle = DataGridViewCellBorderStyle.None;
        }

        /// <summary>
        /// On load wait x seconds for loading screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UIForm_Load(object sender, EventArgs e)
        {
            this.bgWorkerProgressBar.RunWorkerAsync();
        }

        /// <summary>
        /// Show left panel button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void showButton_Click_1(object sender, EventArgs e)
        {
            this.programTabs.SelectedIndex = 0;
            this.bgWorkerProgressBar.RunWorkerAsync();
            this.splitContainer.Panel1Collapsed = false;
        }

        /// <summary>
        /// Hide left panel button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void hideButton_Click_1(object sender, EventArgs e)
        {
            this.programTabs.SelectedIndex = 0;
            this.bgWorkerProgressBar.RunWorkerAsync();
            this.splitContainer.Panel1Collapsed = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void reloadButton_Click_1(object sender, EventArgs e)
        {
            //Tab index for loading
            this.programTabs.SelectedIndex = 0;
            this.bgWorkerProgressBar.RunWorkerAsync();
        }
        /// <summary>
        /// Loading screen
        /// </summary>
        /// <param name="duration">Sleep for x seconds</param>
        private void loadingScreen( int duration ) {
            Thread.Sleep(duration*1000);
        }

        /// <summary>
        /// Background worker for loading screen; does no block UI thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bgWorkerProgressBar_DoWork(object sender, DoWorkEventArgs e)
        {
            loadingScreen(4);
        }

        /// <summary>
        /// Completion of progress bar switch to mainTab 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bgWorkerProgressBar_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //Tab index for mainTab
            this.programTabs.SelectedIndex = 1;
        }




    }
}
