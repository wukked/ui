﻿namespace UI
{
    partial class UIForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.mainTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutBottom = new System.Windows.Forms.TableLayoutPanel();
            this.hideButton = new DevComponents.DotNetBar.ButtonX();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.radioGroupPanel = new System.Windows.Forms.Panel();
            this.radioButton2 = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.radioButton1 = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.textBox1 = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.programTabs = new System.Windows.Forms.TabControl();
            this.loadingTab = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.loadingLbl = new DevComponents.DotNetBar.LabelX();
            this.progressBarX1 = new DevComponents.DotNetBar.Controls.ProgressBarX();
            this.mainTab = new System.Windows.Forms.TabPage();
            this.bgWorkerProgressBar = new System.ComponentModel.BackgroundWorker();
            this.loadingLabel = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.showButton = new DevComponents.DotNetBar.ButtonX();
            this.BSUIIncident = new System.Windows.Forms.BindingSource(this.components);
            this.reloadButton = new DevComponents.DotNetBar.ButtonX();
            this.rightDGV = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.uIIncidentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.statusDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sourceDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.referenceNumDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inquiryReasonDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subjectDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.actionDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mainTableLayout.SuspendLayout();
            this.tableLayoutBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.radioGroupPanel.SuspendLayout();
            this.programTabs.SuspendLayout();
            this.loadingTab.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.mainTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BSUIIncident)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uIIncidentBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // mainTableLayout
            // 
            this.mainTableLayout.ColumnCount = 1;
            this.mainTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.47917F));
            this.mainTableLayout.Controls.Add(this.tableLayoutBottom, 0, 1);
            this.mainTableLayout.Controls.Add(this.splitContainer, 0, 0);
            this.mainTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTableLayout.Location = new System.Drawing.Point(3, 3);
            this.mainTableLayout.Name = "mainTableLayout";
            this.mainTableLayout.RowCount = 2;
            this.mainTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.9465F));
            this.mainTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.053498F));
            this.mainTableLayout.Size = new System.Drawing.Size(943, 453);
            this.mainTableLayout.TabIndex = 0;
            // 
            // tableLayoutBottom
            // 
            this.tableLayoutBottom.ColumnCount = 5;
            this.tableLayoutBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutBottom.Controls.Add(this.hideButton, 1, 0);
            this.tableLayoutBottom.Controls.Add(this.showButton, 2, 0);
            this.tableLayoutBottom.Controls.Add(this.reloadButton, 3, 0);
            this.tableLayoutBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutBottom.Location = new System.Drawing.Point(3, 414);
            this.tableLayoutBottom.Name = "tableLayoutBottom";
            this.tableLayoutBottom.RowCount = 1;
            this.tableLayoutBottom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutBottom.Size = new System.Drawing.Size(937, 36);
            this.tableLayoutBottom.TabIndex = 3;
            // 
            // hideButton
            // 
            this.hideButton.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.hideButton.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.hideButton.Location = new System.Drawing.Point(366, 3);
            this.hideButton.Name = "hideButton";
            this.hideButton.Size = new System.Drawing.Size(64, 23);
            this.hideButton.TabIndex = 3;
            this.hideButton.Text = "Hide";
            this.hideButton.Click += new System.EventHandler(this.hideButton_Click_1);
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(3, 3);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.tableLayoutPanel2);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.rightDGV);
            this.splitContainer.Size = new System.Drawing.Size(937, 405);
            this.splitContainer.SplitterDistance = 400;
            this.splitContainer.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.labelX9, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.labelX8, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.labelX7, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.labelX6, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.labelX5, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.labelX4, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.labelX3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.labelX2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.textBox8, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.textBox7, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.textBox6, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.textBox5, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.textBox4, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.textBox3, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.radioGroupPanel, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.textBox2, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelX1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.textBox1, 1, 8);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 9;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(400, 405);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // labelX9
            // 
            this.labelX9.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelX9.Location = new System.Drawing.Point(122, 331);
            this.labelX9.Name = "labelX9";
            this.labelX9.Size = new System.Drawing.Size(75, 23);
            this.labelX9.TabIndex = 26;
            this.labelX9.Text = "labelX9";
            this.labelX9.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX8
            // 
            this.labelX8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelX8.Location = new System.Drawing.Point(122, 251);
            this.labelX8.Name = "labelX8";
            this.labelX8.Size = new System.Drawing.Size(75, 23);
            this.labelX8.TabIndex = 25;
            this.labelX8.Text = "labelX8";
            this.labelX8.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX7
            // 
            this.labelX7.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelX7.Location = new System.Drawing.Point(122, 216);
            this.labelX7.Name = "labelX7";
            this.labelX7.Size = new System.Drawing.Size(75, 23);
            this.labelX7.TabIndex = 24;
            this.labelX7.Text = "labelX7";
            this.labelX7.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX6
            // 
            this.labelX6.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelX6.Location = new System.Drawing.Point(122, 181);
            this.labelX6.Name = "labelX6";
            this.labelX6.Size = new System.Drawing.Size(75, 23);
            this.labelX6.TabIndex = 23;
            this.labelX6.Text = "longLabel1";
            this.labelX6.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX5
            // 
            this.labelX5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelX5.Location = new System.Drawing.Point(122, 146);
            this.labelX5.Name = "labelX5";
            this.labelX5.Size = new System.Drawing.Size(75, 23);
            this.labelX5.TabIndex = 22;
            this.labelX5.Text = "labelX5";
            this.labelX5.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX4
            // 
            this.labelX4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelX4.Location = new System.Drawing.Point(122, 111);
            this.labelX4.Name = "labelX4";
            this.labelX4.Size = new System.Drawing.Size(75, 23);
            this.labelX4.TabIndex = 21;
            this.labelX4.Text = "labelX4";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX3
            // 
            this.labelX3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelX3.Location = new System.Drawing.Point(122, 76);
            this.labelX3.Name = "labelX3";
            this.labelX3.Size = new System.Drawing.Size(75, 23);
            this.labelX3.TabIndex = 20;
            this.labelX3.Text = "labelX3";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX2
            // 
            this.labelX2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelX2.Location = new System.Drawing.Point(122, 41);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(75, 23);
            this.labelX2.TabIndex = 19;
            this.labelX2.Text = "labelX2";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // textBox8
            // 
            this.textBox8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox8.Location = new System.Drawing.Point(203, 217);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(172, 20);
            this.textBox8.TabIndex = 17;
            // 
            // textBox7
            // 
            this.textBox7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox7.Location = new System.Drawing.Point(203, 182);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(172, 20);
            this.textBox7.TabIndex = 16;
            // 
            // textBox6
            // 
            this.textBox6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox6.Location = new System.Drawing.Point(203, 147);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(172, 20);
            this.textBox6.TabIndex = 15;
            // 
            // textBox5
            // 
            this.textBox5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox5.Location = new System.Drawing.Point(203, 112);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(172, 20);
            this.textBox5.TabIndex = 14;
            // 
            // textBox4
            // 
            this.textBox4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox4.Location = new System.Drawing.Point(203, 77);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(172, 20);
            this.textBox4.TabIndex = 13;
            // 
            // textBox3
            // 
            this.textBox3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox3.Location = new System.Drawing.Point(203, 42);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(172, 20);
            this.textBox3.TabIndex = 12;
            // 
            // radioGroupPanel
            // 
            this.radioGroupPanel.Controls.Add(this.radioButton2);
            this.radioGroupPanel.Controls.Add(this.radioButton1);
            this.radioGroupPanel.Location = new System.Drawing.Point(203, 248);
            this.radioGroupPanel.Name = "radioGroupPanel";
            this.radioGroupPanel.Size = new System.Drawing.Size(172, 29);
            this.radioGroupPanel.TabIndex = 8;
            // 
            // radioButton2
            // 
            this.radioButton2.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.radioButton2.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButton2.Location = new System.Drawing.Point(75, 0);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(75, 29);
            this.radioButton2.TabIndex = 3;
            this.radioButton2.Text = "radio2";
            // 
            // radioButton1
            // 
            this.radioButton1.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.radioButton1.Dock = System.Windows.Forms.DockStyle.Left;
            this.radioButton1.Location = new System.Drawing.Point(0, 0);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(75, 29);
            this.radioButton1.TabIndex = 2;
            this.radioButton1.Text = "radio1";
            // 
            // textBox2
            // 
            this.textBox2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox2.Location = new System.Drawing.Point(203, 7);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(172, 20);
            this.textBox2.TabIndex = 11;
            // 
            // labelX1
            // 
            this.labelX1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.labelX1.Location = new System.Drawing.Point(122, 6);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(75, 23);
            this.labelX1.TabIndex = 18;
            this.labelX1.Text = "labelX1";
            this.labelX1.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // textBox1
            // 
            // 
            // 
            // 
            this.textBox1.Border.Class = "TextBoxBorder";
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(203, 283);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(194, 119);
            this.textBox1.TabIndex = 27;
            // 
            // programTabs
            // 
            this.programTabs.Controls.Add(this.loadingTab);
            this.programTabs.Controls.Add(this.mainTab);
            this.programTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.programTabs.ItemSize = new System.Drawing.Size(50, 20);
            this.programTabs.Location = new System.Drawing.Point(0, 0);
            this.programTabs.Name = "programTabs";
            this.programTabs.SelectedIndex = 0;
            this.programTabs.Size = new System.Drawing.Size(957, 487);
            this.programTabs.TabIndex = 1;
            // 
            // loadingTab
            // 
            this.loadingTab.Controls.Add(this.tableLayoutPanel1);
            this.loadingTab.Location = new System.Drawing.Point(4, 24);
            this.loadingTab.Name = "loadingTab";
            this.loadingTab.Padding = new System.Windows.Forms.Padding(3);
            this.loadingTab.Size = new System.Drawing.Size(949, 459);
            this.loadingTab.TabIndex = 1;
            this.loadingTab.Text = "Loading";
            this.loadingTab.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Controls.Add(this.loadingLbl, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.progressBarX1, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(87, 87);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(804, 260);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // loadingLbl
            // 
            this.loadingLbl.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.loadingLbl.Location = new System.Drawing.Point(364, 148);
            this.loadingLbl.Name = "loadingLbl";
            this.loadingLbl.Size = new System.Drawing.Size(75, 14);
            this.loadingLbl.TabIndex = 1;
            this.loadingLbl.Text = "Loading...";
            // 
            // progressBarX1
            // 
            this.progressBarX1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBarX1.Location = new System.Drawing.Point(53, 98);
            this.progressBarX1.Name = "progressBarX1";
            this.progressBarX1.ProgressType = DevComponents.DotNetBar.eProgressItemType.Marquee;
            this.progressBarX1.Size = new System.Drawing.Size(698, 44);
            this.progressBarX1.TabIndex = 0;
            this.progressBarX1.Text = "progressBarX1";
            // 
            // mainTab
            // 
            this.mainTab.Controls.Add(this.mainTableLayout);
            this.mainTab.Location = new System.Drawing.Point(4, 24);
            this.mainTab.Name = "mainTab";
            this.mainTab.Padding = new System.Windows.Forms.Padding(3);
            this.mainTab.Size = new System.Drawing.Size(949, 459);
            this.mainTab.TabIndex = 0;
            this.mainTab.Text = "Main";
            this.mainTab.UseVisualStyleBackColor = true;
            // 
            // bgWorkerProgressBar
            // 
            this.bgWorkerProgressBar.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWorkerProgressBar_DoWork);
            this.bgWorkerProgressBar.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWorkerProgressBar_RunWorkerCompleted);
            // 
            // loadingLabel
            // 
            this.loadingLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.loadingLabel.AutoSize = true;
            this.loadingLabel.Location = new System.Drawing.Point(375, 178);
            this.loadingLabel.Name = "loadingLabel";
            this.loadingLabel.Size = new System.Drawing.Size(54, 13);
            this.loadingLabel.TabIndex = 1;
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.progressBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBar1.Location = new System.Drawing.Point(0, 0);
            this.progressBar1.MarqueeAnimationSpeed = 1;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(698, 44);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 0;
            // 
            // showButton
            // 
            this.showButton.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.showButton.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.showButton.Location = new System.Drawing.Point(436, 3);
            this.showButton.Name = "showButton";
            this.showButton.Size = new System.Drawing.Size(64, 23);
            this.showButton.TabIndex = 4;
            this.showButton.Text = "Show";
            this.showButton.Click += new System.EventHandler(this.showButton_Click_1);
            // 
            // BSUIIncident
            // 
            this.BSUIIncident.DataSource = typeof(UI.UIIncident);
            // 
            // reloadButton
            // 
            this.reloadButton.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.reloadButton.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.reloadButton.Location = new System.Drawing.Point(506, 3);
            this.reloadButton.Name = "reloadButton";
            this.reloadButton.Size = new System.Drawing.Size(64, 23);
            this.reloadButton.TabIndex = 5;
            this.reloadButton.Text = "Reload";
            this.reloadButton.Click += new System.EventHandler(this.reloadButton_Click_1);
            // 
            // rightDGV
            // 
            this.rightDGV.AutoGenerateColumns = false;
            this.rightDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rightDGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.statusDataGridViewTextBoxColumn1,
            this.sourceDataGridViewTextBoxColumn1,
            this.referenceNumDataGridViewTextBoxColumn1,
            this.typeDataGridViewTextBoxColumn1,
            this.inquiryReasonDataGridViewTextBoxColumn1,
            this.subjectDataGridViewTextBoxColumn1,
            this.actionDataGridViewTextBoxColumn1});
            this.rightDGV.DataSource = this.uIIncidentBindingSource;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.rightDGV.DefaultCellStyle = dataGridViewCellStyle1;
            this.rightDGV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rightDGV.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.rightDGV.Location = new System.Drawing.Point(0, 0);
            this.rightDGV.Name = "rightDGV";
            this.rightDGV.Size = new System.Drawing.Size(533, 405);
            this.rightDGV.TabIndex = 1;
            // 
            // uIIncidentBindingSource
            // 
            this.uIIncidentBindingSource.DataSource = typeof(UI.UIIncident);
            // 
            // statusDataGridViewTextBoxColumn1
            // 
            this.statusDataGridViewTextBoxColumn1.DataPropertyName = "status";
            this.statusDataGridViewTextBoxColumn1.HeaderText = "status";
            this.statusDataGridViewTextBoxColumn1.Name = "statusDataGridViewTextBoxColumn1";
            // 
            // sourceDataGridViewTextBoxColumn1
            // 
            this.sourceDataGridViewTextBoxColumn1.DataPropertyName = "source";
            this.sourceDataGridViewTextBoxColumn1.HeaderText = "source";
            this.sourceDataGridViewTextBoxColumn1.Name = "sourceDataGridViewTextBoxColumn1";
            // 
            // referenceNumDataGridViewTextBoxColumn1
            // 
            this.referenceNumDataGridViewTextBoxColumn1.DataPropertyName = "referenceNum";
            this.referenceNumDataGridViewTextBoxColumn1.HeaderText = "referenceNum";
            this.referenceNumDataGridViewTextBoxColumn1.Name = "referenceNumDataGridViewTextBoxColumn1";
            // 
            // typeDataGridViewTextBoxColumn1
            // 
            this.typeDataGridViewTextBoxColumn1.DataPropertyName = "type";
            this.typeDataGridViewTextBoxColumn1.HeaderText = "type";
            this.typeDataGridViewTextBoxColumn1.Name = "typeDataGridViewTextBoxColumn1";
            // 
            // inquiryReasonDataGridViewTextBoxColumn1
            // 
            this.inquiryReasonDataGridViewTextBoxColumn1.DataPropertyName = "inquiryReason";
            this.inquiryReasonDataGridViewTextBoxColumn1.HeaderText = "inquiryReason";
            this.inquiryReasonDataGridViewTextBoxColumn1.Name = "inquiryReasonDataGridViewTextBoxColumn1";
            // 
            // subjectDataGridViewTextBoxColumn1
            // 
            this.subjectDataGridViewTextBoxColumn1.DataPropertyName = "subject";
            this.subjectDataGridViewTextBoxColumn1.HeaderText = "subject";
            this.subjectDataGridViewTextBoxColumn1.Name = "subjectDataGridViewTextBoxColumn1";
            // 
            // actionDataGridViewTextBoxColumn1
            // 
            this.actionDataGridViewTextBoxColumn1.DataPropertyName = "action";
            this.actionDataGridViewTextBoxColumn1.HeaderText = "action";
            this.actionDataGridViewTextBoxColumn1.Name = "actionDataGridViewTextBoxColumn1";
            // 
            // UIForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(957, 487);
            this.Controls.Add(this.programTabs);
            this.Name = "UIForm";
            this.Text = "UI Assignment";
            this.Load += new System.EventHandler(this.UIForm_Load);
            this.mainTableLayout.ResumeLayout(false);
            this.tableLayoutBottom.ResumeLayout(false);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.radioGroupPanel.ResumeLayout(false);
            this.programTabs.ResumeLayout(false);
            this.loadingTab.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.mainTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BSUIIncident)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uIIncidentBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainTableLayout;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.TabControl programTabs;
        private System.Windows.Forms.TabPage mainTab;
        private System.ComponentModel.BackgroundWorker bgWorkerProgressBar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutBottom;
        private System.Windows.Forms.TabPage loadingTab;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label loadingLabel;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.BindingSource BSUIIncident;
        private DevComponents.DotNetBar.LabelX loadingLbl;
        private DevComponents.DotNetBar.Controls.ProgressBarX progressBarX1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX6;
        private System.Windows.Forms.Panel radioGroupPanel;
        private DevComponents.DotNetBar.Controls.CheckBoxX radioButton2;
        private DevComponents.DotNetBar.Controls.CheckBoxX radioButton1;
        private DevComponents.DotNetBar.Controls.TextBoxX textBox1;
        private DevComponents.DotNetBar.ButtonX hideButton;
        private DevComponents.DotNetBar.ButtonX showButton;
        private DevComponents.DotNetBar.ButtonX reloadButton;
        private DevComponents.DotNetBar.Controls.DataGridViewX rightDGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn statusDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn sourceDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn referenceNumDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn inquiryReasonDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn subjectDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn actionDataGridViewTextBoxColumn1;
        private System.Windows.Forms.BindingSource uIIncidentBindingSource;
    }
}

