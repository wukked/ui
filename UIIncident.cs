﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI
{
    /// <summary>
    /// UI Incident class object to bind to datasource for dgv
    /// </summary>
    public class UIIncident
    {
        public string status { get; set; }
        public string source { get; set; }
        public int referenceNum { get; set; }
        public string type { get; set; }
        public string inquiryReason { get; set; }
        public string subject { get; set; }
        public string action { get; set; }
    }
}
